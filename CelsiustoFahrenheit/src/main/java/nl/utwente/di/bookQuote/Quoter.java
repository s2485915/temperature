package nl.utwente.di.bookQuote;

public class Quoter {

    public double convert(String isbn){

        return Double.parseDouble(isbn)/2 + 30;
    }
}
